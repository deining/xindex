local data = "àbcdéêèf"
--local u = require("utf8")
local udata = utf8.codes(data)

print(type(data), data) -- the orignal print(type(udata), udata) -- automatic conversion to string

print(#data) -- is not the good number of printed characters on screen print(#udata) -- is the number of printed characters on screen

print(utf8.codes(string.sub(data,4,8))) -- be able to use the sub() like a string
