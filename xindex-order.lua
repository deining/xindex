-----------------------------------------------------------------------
--         FILE:  xindex-order.lua
--  DESCRIPTION:  configuration file for xindex.lua
-- REQUIREMENTS:  
--       AUTHOR:  Herbert Voß
--      LICENSE:  LPPL1.3
--
-- $Id: xindex-order.lua 22 2022-02-07 12:18:15Z hvoss $
-----------------------------------------------------------------------

if not modules then modules = { } end modules ['xindex-order'] = {
      version = 0.62,
      comment = "main configuration to xindex.lua",
       author = "Herbert Voss",
    copyright = "Herbert Voss",
      license = "LPPL 1.3"
}

--[[
output_order = { 
  Greek =    { 'α', 'β', 'γ', 'δ', 'ε', 'ζ', 'η', 'θ', 'ι', 'κ', 'λ', 'μ', 'ν', 'ξ', 'ο', 'π', 'ρ', 'σ', 'τ', 'υ', 'φ', 'χ', 'ψ', 'ω' },
  Latin =    { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' },
  Kyrillic = { 'а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я' },
  GREEK =    { 'Α', 'Β', 'Γ', 'Δ', 'Ε', 'Ζ', 'Η', 'Θ', 'Ι', 'Κ', 'Λ', 'Μ', 'Ν', 'Ξ', 'Ο', 'Π', 'Ρ', 'Σ', 'Τ', 'Υ', 'Φ', 'Χ', 'Ψ', 'Ω' },
  LATIN =    { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' },
  KYRILLIC = { 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я' }
}
]]

output_order = { 
  latin = { min = 0x041, max = 0x024F},
  greek = { min = 0x370, max = 0x03FF},
  cyrillic = { min = 0x400, max = 0x052F},
  symbols = {},
  numbers = {},
}

function SORTendhook(list)
  print ("We have "..#list.." total list entries")
  local greek = {}
  local latin = {}
  local cyrillic = {}
  local symbols = {}
  local numbers = {}
  local others = {}
  local firstChar, charType
  local firstCharNumber
  local v
  for i=1,#list do
    v = list[i]
    firstChar = utf.sub(v["sortChar"],1,1)
    firstCharNumber = string.utfvalue(firstChar)
    charType = getCharType(firstChar)
    if charType == 0 then 
      symbols[#symbols+1] = v
    elseif charType == 1 then 
      numbers[#numbers+1] = v
    elseif 
      if firstCharNumber > 0x052F then
        others[#others+1] = v
      elseif firstCharNumber >= 0x0400 then
        cyrillic[#cyrillic+1] = v
      elseif firstCharNumber <= 0x03FF then
        if firstCharNumber >= 0x0370 then
          greek[#greek+1] = v
        elseif firstCharNumber <= 0x024F then
          if firstCharNumber >= 0x041 then
            latin[#latin+1] = v
          else
            others[#others+1] = v
          end
        end
      end
    end
  end
  print ("We have "..#greek.." Greek entries")
  print ("We have "..#latin.." Latin entries")
  print ("We have "..#cyrillic.." Cyrillic entries")
  print ("We have "..#symbols.." Symbols entries")
  print ("We have "..#numbers.." Number entries")
  print ("We have "..#others.." other entries")
  list = {}
  for i = 1,#greek do list[#list+1] = greek[i] end
  for i = 1,#latin do list[#list+1] = latin[i] end
  for i = 1,#cyrillic do list[#list+1] = cyrillic[i] end
  for i = 1,#symbols do list[#list+1] = symbols[i] end
  for i = 1,numbers do list[#list+1] = numbers[i] end
  for i = 1,#others do list[#list+1] = others[i] end
  return list
end
