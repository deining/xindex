-----------------------------------------------------------------------
--         FILE:  xindex-numeric.lua
--  DESCRIPTION:  configuration file for xindex.lua, sort V64 before V128
-- REQUIREMENTS:  
--       AUTHOR:  Herbert Voß
--      LICENSE:  LPPL1.3
--
-- configuration for index files with ignoring alpha characters
--
-- $Id: xindex-numeric.lua 22 2022-02-07 12:18:15Z hvoss $

if not modules then modules = { } end modules ['xindex-numeric'] = {
      version = 0.62,
      comment = "configuration to xindex.lua",
       author = "Herbert Voss",
    copyright = "Herbert Voss",
      license = "LPPL 1.3"
}

--local version = "0.01"

itemPageDelimiter = " \\dotfill "     -- Hello .....  14
compressPages     = true    -- something like 12--15, instead of 12,13,14,15. the |( ... |) syntax is still valid
fCompress	  = true    -- 3f -> page 3, 4 and 3ff -> page 3, 4, 5
minCompress       = 3       -- 14--17 or 
numericPage       = false   -- for non numerical pagenumbers, like "V17"
sublabels         = {"", "-\\,", "--\\,", "---\\,"} -- for the (sub(sub(sub-items  first one is for item
pageNoPrefixDel   = ""     -- a delimiter for page numbers like "VI-17"
indexOpening      = ""     -- commands after \begin{theindex}
rangeSymbol       = "--"
idxnewletter      = "\\textbf"  -- Only valid if -n is not set

case_sensitive    = false  -- speeds up running

folium = { 
  de = {"f.", "ff."},
  en = {"f.", "ff."},
  fr = {"\\,sq.","\\,sqq."},
}


--[[
    Each character's position in this array-like table determines its 'priority'.
    Several characters in the same slot have the same 'priority'.
]]
alphabet_lower = { --   for sorting
    { ' ' },  -- only for internal tests
    { 'a', 'á', 'à', 'ä'},
    { 'b' },
    { 'c' },
    { 'd' },
    { 'e', 'é', 'è', 'ë' },
    { 'f' },
    { 'g' },
    { 'h' },
    { 'i', 'í', 'ì', 'ï' },
    { 'j' },
    { 'k' },
    { 'l' },
    { 'm' },
    { 'n', 'ñ' },
    { 'o', 'ó', 'ò', 'ö' },
    { 'p' },
    { 'q' },
    { 'r' },
    { 's' },
    { 't' },
    { 'u', 'ú', 'ù', 'ü' },
    { 'v' },
    { 'w' },
    { 'x' },
    { 'y' },
    { 'z' }
}
alphabet_upper = { -- for sorting
    { ' ' },
    { 'A', 'Á', 'À', 'Ä'},
    { 'B' },
    { 'C' },
    { 'D' },
    { 'E', 'È', 'È', 'ë' },
    { 'F' },
    { 'G' },
    { 'H' },
    { 'I', 'Í', 'Ì', 'ï' },
    { 'J' },
    { 'K' },
    { 'L' },
    { 'M' },
    { 'N', 'Ñ' },
    { 'O', 'Ó', 'Ò', 'Ö' },
    { 'P' },
    { 'Q' },
    { 'R' },
    { 'S' },
    { 'T' },
    { 'U', 'Ú', 'Ù', 'Ü' },
    { 'V' },
    { 'W' },
    { 'X' },
    { 'Y' },
    { 'Z' }
}

function specialCompressPageList(pages)
--  print(#pages..".. number:|"..pages[1]["number"].."| Special:"..pages[1]["special"])
  if (pages[1]["number"] == "") then pages[1]["number"] = " " end
  if (#pages <= 1) then 
    pages[1]["number"] = pages[1]["number"]:gsub('-',':~')-- replace "-" with ":~"
    return pages 
  end  -- only one pageno

  local sortPages = {}
  local page
  local i
--print("----------------------------------------")
  for i=1,#pages do
     page = string.gsub(pages[i]["number"],'%a*','') -- only the number V64->64
--     page = string.format("%5s",page)
     sortPages[#sortPages+1] = {
       origin = pages[i],
       sort = page }  -- no letters
  end
-- sort the page list  
  table.sort(sortPages, function(a,b) return tonumber(a["sort"]) < tonumber(b["sort"]) end )
  local Pages = {}
--  writeLog(1,print(getRawPagesP(sortPages)),2)
  for i=1,#sortPages do    -- use the sorted origin table
    Pages[#Pages+1] = sortPages[i]["origin"]
  end
--  writeLog(1,print(getRawPagesP(Pages)),2)
-- test if two or more pages in the list
  return Pages
end

function colorBox(str)
  return ("\\colorbox{black!15}{"..str.."}:~")
end


function specialGetPageList(v,hyperpage)
  local Pages = {}
  if v["pages"] then
    table.sort(v["pages"],pageCompare)-- nur nötig, da User manuell eine Zeile einfügen kann
    if specialCompressPageList then
      Pages = specialCompressPageList(v["pages"])
    else
      Pages = compressPageList(v["pages"])
    end
--  require 'xindex-pretty'.dump(Pages)   -- only for internal dump
    local pageNo
    if hyperpage then
        if string.find(v["pages"][1]["special"],"hyperindexformat") then
          pageNo = v["pages"][1]["special"].."{"..checkFF(Pages[1]["number"].."}")
        else
          pageNo = "\\hyperpage{"..checkFF(Pages[1]["number"]).."}"
        end
      for i=2,#Pages do
        if string.find(v["pages"][i]["special"],"hyperindexformat") then
          pageNo = pageNo..", "..v["pages"][i]["special"].."{"..checkFF(Pages[i]["number"].."}")
        else
          pageNo = pageNo..", \\hyperpage{"..checkFF(Pages[i]["number"]).."}"
        end
--        Pages[i] = nil
      end
    else
      writeLog(1,"getPageList: "..tostring(Pages[1]["special"]).."{"..tostring(Pages[1]["number"]).."}\n",2) 
      if (Pages[1]["special"] == nil) or (Pages[1]["number"] == nil) then return ""  end 
      pageNo = Pages[1]["special"].."{"..Pages[1]["number"]:gsub('(.-):~',colorBox).."}"  
      for i=2,#Pages do
        if Pages[i]["number"] then
          pageNo = pageNo..", "..Pages[i]["special"].."{"..Pages[i]["number"]:gsub('(.-):~',colorBox).."}"
          Pages[i] = {}
        end
      end
    end
    return pageNo
  else
    return ""
  end
end

